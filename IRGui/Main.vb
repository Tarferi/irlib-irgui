﻿Imports IRectLib

Public Class Main

    Private IRLib As IRectLib.IRInstance = IRectLib.IRFactory.createInstance()
    Private LibCallbacks As UICallbacks = New UICallbacks()

    Private scr_login As LoginUI = New LoginUI(IRLib, LibCallbacks, Me)
    Private scr_chat As ChatUI = New ChatUI(IRLib, LibCallbacks, Me)

    Private Shadows activeForm As ContainerControl = Nothing


    Public Sub New()
        InitializeComponent()
        addForm(scr_login)
        addForm(scr_chat)
        setLoginScreen()
        'setChatScreen(IRLib.tryLogin("Tarferi", "test", "asdsdf", LibCallbacks))
    End Sub

    Public Sub setChatScreen(iRServer As IRServer)
        setActiveForm(scr_chat)
        scr_chat.init(Me)
        scr_chat.bindData(iRServer)
    End Sub

    Private Sub addForm(frm As ContainerControl)
        Me.Controls.Add(frm)
        frm.Hide()
    End Sub

    Private Sub setActiveForm(frm As ContainerControl)
        If Not activeForm Is Nothing Then
            activeForm.Hide()
        Else
            root.Hide()
        End If
        activeForm = frm
        activeForm.Show()
    End Sub

    Public Sub updateWindow(width As Integer, height As Integer, text As String, resizable As Boolean)
        Me.Text = text
        Me.MinimizeBox = resizable
        Me.MaximizeBox = resizable
        Me.Width = width
        Me.Height = height + 40
        If resizable Then
            Me.FormBorderStyle = FormBorderStyle.Sizable
        Else
            Me.FormBorderStyle = FormBorderStyle.Fixed3D
        End If

    End Sub

    Public Sub setLoginScreen()
        setActiveForm(scr_login)
        scr_login.init(Me)
    End Sub


    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs)

    End Sub


    Public Class UICallbacks
        Inherits IRCallbackCollection

        Public Sub New()
            MyBase.New(New UIMessageListener(), New UIFriendshipRequestListener(), New UIStatusListener())
        End Sub
    End Class

    '
    '
    '             CALLBACK INTERFACES
    '
    '
    Private Class UIFriendshipRequestListener
        Implements IRFrienshipRequestListener

        Public Sub IRFriendshipRequestReceived(request As IRFriendshipRequest) Implements IRFrienshipRequestListener.IRFriendshipRequestReceived
            Throw New NotImplementedException()
        End Sub
    End Class


    Private Class UIMessageListener
        Implements IRMessageListener

        Public Sub IRMessageReceived(chat As IRChat, user As IRUser, receiveTime As Date, messageContents As String) Implements IRMessageListener.IRMessageReceived
            Dim chatui As chatpanel = chat.getDisplayObject()
            chatui.messageReceived(user, receiveTime, messageContents)
        End Sub
    End Class

    Private Class UIStatusListener
        Implements IRUserStatusChangeListener

        Public Sub IRUserStatusChanged(user As IRUser, newStatus As IRUserStatus) Implements IRUserStatusChangeListener.IRUserStatusChanged
            Dim userui As WpfListUserItem = user.getDisplayObject()
            userui.dataChanged(user)
        End Sub
    End Class

End Class