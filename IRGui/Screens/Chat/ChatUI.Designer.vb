﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ChatUI
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent(chatui As ChatUI)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.LeftPanel1 = New IRGui.leftPanel(chatui)
        Me.centerPanel = New System.Windows.Forms.Panel()
        Me.pnlChat = New System.Windows.Forms.Panel()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.centerPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Panel1.Controls.Add(Me.LeftPanel1)
        Me.SplitContainer1.Panel1MinSize = 300
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Panel2.Controls.Add(Me.pnlChat)
        Me.SplitContainer1.Panel2MinSize = 500
        Me.SplitContainer1.Size = New System.Drawing.Size(934, 521)
        Me.SplitContainer1.SplitterDistance = 311
        Me.SplitContainer1.TabIndex = 0
        '
        'LeftPanel1
        '
        Me.LeftPanel1.BackColor = System.Drawing.Color.Transparent
        Me.LeftPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LeftPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LeftPanel1.Location = New System.Drawing.Point(0, 0)
        Me.LeftPanel1.MinimumSize = New System.Drawing.Size(300, 4)
        Me.LeftPanel1.Name = "LeftPanel1"
        Me.LeftPanel1.Padding = New System.Windows.Forms.Padding(5)
        Me.LeftPanel1.Size = New System.Drawing.Size(311, 521)
        Me.LeftPanel1.TabIndex = 0
        '
        'centerPanel
        '
        Me.centerPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.centerPanel.BackColor = System.Drawing.Color.Transparent
        Me.centerPanel.Controls.Add(Me.SplitContainer1)
        Me.centerPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.centerPanel.Location = New System.Drawing.Point(0, 0)
        Me.centerPanel.Name = "centerPanel"
        Me.centerPanel.Size = New System.Drawing.Size(934, 521)
        Me.centerPanel.TabIndex = 0
        '
        'pnlChat
        '
        Me.pnlChat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlChat.Location = New System.Drawing.Point(0, 0)
        Me.pnlChat.Name = "pnlChat"
        Me.pnlChat.Size = New System.Drawing.Size(619, 521)
        Me.pnlChat.TabIndex = 0
        '
        'ChatUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.centerPanel)
        Me.Name = "ChatUI"
        Me.Size = New System.Drawing.Size(934, 521)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.centerPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents centerPanel As Panel
    Friend WithEvents LeftPanel1 As leftPanel
    Friend WithEvents pnlChat As Panel
End Class
