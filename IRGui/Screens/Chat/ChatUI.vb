﻿Imports IRectLib
Imports IRGui

Public Class ChatUI
    Private iRLib As IRInstance
    Private libCallbacks As Main.UICallbacks
    Private main As Main

    Public Sub New(iRLib As IRInstance, libCallbacks As Main.UICallbacks, main As Main)
        Me.iRLib = iRLib
        Me.libCallbacks = libCallbacks
        Me.main = main
        InitializeComponent(Me)
    End Sub

    Friend Sub init(main As Main)
        main.updateWindow(Me.Width, Me.Height, "IR Chat", True)
        Me.Dock = DockStyle.Fill
    End Sub

    Friend Sub bindData(iRServer As IRServer)
        LeftPanel1.bindData(iRServer)
    End Sub

    Private activeChatPanel As chatpanel = Nothing

    Public Sub setActiveChat(chat As IRChat)
        If activeChatPanel IsNot Nothing Then
            pnlChat.Controls.Remove(activeChatPanel)
        End If
        If chat Is Nothing Then
            activeChatPanel = Nothing
        Else
            activeChatPanel = chat.getDisplayObject()
            pnlChat.Controls.Add(activeChatPanel)
            activeChatPanel.Dock = DockStyle.Fill
        End If

    End Sub

    Public Function isActiveChat(chatpanel As chatpanel) As Boolean
        Return chatpanel.Equals(isActiveChat)
    End Function
End Class
