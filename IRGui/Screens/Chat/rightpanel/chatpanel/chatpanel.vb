﻿Imports IRectLib

Public Class chatpanel
    Implements IRDataChangedListener

    Private chat As IRChat
    Private chatName As String = Nothing
    Private chatui As ChatUI

    Public Sub New(chat As IRChat, chatui As ChatUI)
        InitializeComponent()
        Me.chat = chat
        Me.chatui = chatui
        lblName.Text = chat.getChatName()
        chat.setDisplayObject(Me)
    End Sub

    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        sendCurrentText()
    End Sub

    Public Sub sendCurrentText()
        Dim txt As String = txtIn.Text.Trim()
        If txt.Length > 0 Then
            txtIn.Text = ""
            chat.sendMessage(txt)
        End If
    End Sub

    Public Sub dataChanged(data As IRDisplayable) Implements IRDataChangedListener.dataChanged
        If Not chatName.Equals(chat.getChatName()) Then
            chatName = chat.getChatName()
            lblName.Text = chatName
        End If
    End Sub

    Friend Sub messageReceived(user As IRUser, receiveTime As Date, messageContents As String)
        txtOut.AppendText("[" & receiveTime.ToShortTimeString & "] " & user.getDisplayName() & ": " & messageContents & vbNewLine)
        txtOut.SelectionStart = txtOut.Text.Length
        txtOut.ScrollToCaret()
        Dim quser As WpfListUserItem = user.getDisplayObject()
        quser.setNewMessage(Me.chatui.isActiveChat(Me))
    End Sub

    Private Sub txtIn_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIn.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            If Not My.Computer.Keyboard.ShiftKeyDown Then
                txtIn.Text = txtIn.Text.Trim()
                sendCurrentText()
            End If
        ElseIf e.KeyChar = chrW(Keys.Escape) Then
            txtIn.Text = ""
        End If
    End Sub
End Class
