﻿Imports IRectLib
Imports userrList
Imports System.Windows.Forms.Integration


Public Class UserList
    Private server As IRServer
    Private friends As List(Of IRUser)
    Private chatui As ChatUI

    Private qlist As WpfListUser

    Sub New(chatui As ChatUI)
        InitializeComponent()
        Me.chatui = chatui
        qlist = New WpfListUser(chatui)
        elemHost.Child = qlist
    End Sub

    Sub addUser(user As IRUser)
        If user.getDisplayObject Is Nothing Then
            qlist.addUser(user)
        End If

    End Sub

    Sub bindData(iRServer As IRServer)
        Me.server = iRServer
        friendsLoader.RunWorkerAsync()
    End Sub

    Private Sub friendsLoader_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles friendsLoader.DoWork
        friends = server.getAllFriends()
    End Sub

    Private Sub friendsLoader_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles friendsLoader.RunWorkerCompleted
        friends.ForEach(AddressOf addUser)
    End Sub

End Class
