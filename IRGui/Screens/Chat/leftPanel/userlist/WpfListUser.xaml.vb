﻿Imports IRectLib
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.Windows.Media

Public Class WpfListUser

    Private Class ListOfUsers
        Inherits ObservableCollection(Of WpfListUserItem)
    End Class

    Private chatui As ChatUI

    Dim myItems As ListOfUsers = New ListOfUsers()

    Public Sub New(chatui As ChatUI)
        Me.chatui = chatui
        InitializeComponent()
        myList.ItemsSource = myItems
    End Sub

    Public Sub addUser(user As IRUser)
        Dim quser As WpfListUserItem = New WpfListUserItem(user, chatui)
        user.setDisplayObject(quser)
        myItems.Add(quser)
        quser.dataChanged(user)
    End Sub

    Private Sub myList_SelectionChanged(sender As Object, e As Windows.Controls.SelectionChangedEventArgs) Handles myList.SelectionChanged
        Dim itemsRaw As Object = myList.SelectedItems
        Dim count As Integer = itemsRaw.Count
        If count > 1 Then ' Multiple selection
            Dim items(count) As WpfListUserItem
            For i As Integer = 0 To count - 1
                items(i) = itemsRaw(i)
            Next
        ElseIf count = 1 Then ' Single selection
            Dim item As WpfListUserItem = itemsRaw(0)
            item.activateChat()
        Else ' Nothing selected
            chatui.setActiveChat(Nothing)
        End If
    End Sub
End Class

Public Class WpfListUserItem
    Implements IRDataChangedListener
    Implements INotifyPropertyChanged

    Private user As IRUser
    Private chat As chatpanel
    Private chatui As ChatUI

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub activateChat()
        chatui.setActiveChat(user.getChat())
        HadNewMessage = False
    End Sub

    Sub New(user As IRUser, chatui As ChatUI)
        Me.chatui = chatui
        Me.user = user
        Me.chat = New chatpanel(user.getChat(), chatui)
    End Sub

    Private lastDisplayName As String = ""
    Private lastDescription As String = ""
    Private lastMessageD As DateTime = Nothing
    Private lastAvatarIcon As ImageSource = IRImageLoader.getDefaultAvatar()
    Private lastStatusIcon As ImageSource = IRImageLoader.getStatusIcon(IRUserStatus.OFFLINE)
    Private HadNewMessage As Boolean = False

    Public Sub setNewMessage(nm As Boolean)
        HadNewMessage = nm
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("LastMessage"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("HasNewMessage"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("HasNotNewMessage"))
    End Sub

    Public Sub dataChanged(data As IRDisplayable) Implements IRDataChangedListener.dataChanged
        If Not user.getDisplayName.Equals(lastDisplayName) Then
            lastDisplayName = user.getDisplayName()
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("DisplayName"))
        End If
        If Not user.getDescription.Equals(lastDescription) Then
            lastDescription = user.getDescription()
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Description"))
        End If
        If Not user.getLastMessageDate.Equals(lastMessageD) Then
            lastMessageD = user.getLastMessageDate()
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("LastMessage"))
        End If
        If Not user.getAvatar.Equals(lastAvatarIcon) Then
            lastAvatarIcon = user.getAvatar()
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("AvatarIcon"))
        End If
        If Not user.getStatusIcon.Equals(lastStatusIcon) Then
            lastStatusIcon = user.getStatusIcon()
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("StatusIcon"))
        End If
    End Sub

    ReadOnly Property DisplayName As String
        Get
            Return lastDisplayName
        End Get
    End Property

    ReadOnly Property Description As String
        Get
            Return lastDescription
        End Get
    End Property

    ReadOnly Property LastMessage As String
        Get
            Return lastMessageD.ToShortDateString()
        End Get
    End Property

    ReadOnly Property AvatarIcon As ImageSource
        Get
            Return lastAvatarIcon
        End Get
    End Property

    ReadOnly Property StatusIcon As ImageSource
        Get
            Return lastStatusIcon
        End Get
    End Property

    ReadOnly Property HasNewMessage As String
        Get
            If HadNewMessage Then
                Return "Visible"
            Else
                Return "Hidden"
            End If
        End Get
    End Property

    ReadOnly Property HasNotNewMessage As String
        Get
            If HadNewMessage Then
                Return "Hidden"
            Else
                Return "Visible"
            End If
        End Get
    End Property
End Class