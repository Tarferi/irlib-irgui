﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class leftPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent(chatui As ChatUI)
        Me.UserList1 = New IRGui.UserList(chatui)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UserList1
        '
        Me.UserList1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.UserList1.BackColor = System.Drawing.Color.Transparent
        Me.UserList1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UserList1.Location = New System.Drawing.Point(0, 0)
        Me.UserList1.Name = "UserList1"
        Me.UserList1.Padding = New System.Windows.Forms.Padding(5)
        Me.UserList1.Size = New System.Drawing.Size(383, 599)
        Me.UserList1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.UserList1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(5, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(387, 603)
        Me.Panel1.TabIndex = 1
        '
        'leftPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Controls.Add(Me.Panel1)
        Me.Name = "leftPanel"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.Size = New System.Drawing.Size(397, 613)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents UserList1 As UserList
    Friend WithEvents Panel1 As Panel
End Class
