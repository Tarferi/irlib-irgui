﻿Imports System.ComponentModel
Imports IRectLib
Imports IRGui

Public Class LoginUI
    Private iRLib As IRInstance

    Private loginUsername As String
    Private loginPassword As String


    Private libCallbacks As Main.UICallbacks
    Private main As Main

    Public Sub New(iRLib As IRInstance, main As Main)
        Me.iRLib = iRLib
        Me.main = main
        InitializeComponent()
    End Sub


    Public Sub New(iRLib As IRInstance, libCallbacks As Main.UICallbacks, main As Main)
        Me.New(iRLib, main)
        Me.libCallbacks = libCallbacks
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        setControlsLocked(True)
        setProgress(True)
        loginUsername = textUsername.Text
        loginPassword = textPassword.Text
        loginWorker.RunWorkerAsync()
    End Sub

    Private Sub BtnRegister_Click(sender As Object, e As EventArgs) Handles BtnRegister.Click
        setControlsLocked(True)
        setProgress(True)
        loginUsername = textUsername.Text
        loginPassword = textPassword.Text
        RegisterWorker.RunWorkerAsync()
    End Sub

    Private Sub setControlsLocked(lock As Boolean)
        textUsername.Enabled = Not lock
        textPassword.Enabled = Not lock
        btnLogin.Enabled = Not lock
        BtnRegister.Enabled = Not lock

    End Sub

    Private Sub setProgress(progress As Boolean)
        lblError.Visible = False
        loginProgress.Visible = progress
    End Sub

    Private Sub showError(errorText As String)
        loginProgress.Visible = False
        lblError.Visible = True
        lblError.Text = errorText
        setControlsLocked(False)
    End Sub

    Friend Sub init(window As Main)
        window.updateWindow(Me.Width, Me.Height, "Login", True)
        textUsername.Focus()
    End Sub

    Private Lserver As IRLoginRequestResult

    Private Sub loginWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles loginWorker.DoWork
        Lserver = iRLib.tryLogin(loginUsername, loginPassword, libCallbacks)
    End Sub

    Private Sub loginWorker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles loginWorker.RunWorkerCompleted
        If Not (Lserver.isOk()) Then
            showError(Lserver.getErrorString())
        Else
            setProgress(False)
            main.setChatScreen(Lserver.getIRServer())
        End If
    End Sub

    Private LRes As IRRegisterRequestResult

    Private Sub RegisterWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles RegisterWorker.DoWork
        LRes = iRLib.tryRegister(loginUsername, loginPassword)
    End Sub

    Private Sub RegisterWorker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles RegisterWorker.RunWorkerCompleted
        If Not LRes.isOk() Then
            showError(LRes.getErrorString)
        Else
            loginWorker.RunWorkerAsync()
        End If
    End Sub
End Class
