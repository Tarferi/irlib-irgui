﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginUI
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnRegister = New System.Windows.Forms.Button()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.textPassword = New System.Windows.Forms.TextBox()
        Me.textUsername = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.loginProgress = New System.Windows.Forms.ProgressBar()
        Me.loginWorker = New System.ComponentModel.BackgroundWorker()
        Me.RegisterWorker = New System.ComponentModel.BackgroundWorker()
        Me.SuspendLayout()
        '
        'BtnRegister
        '
        Me.BtnRegister.Location = New System.Drawing.Point(163, 104)
        Me.BtnRegister.Name = "BtnRegister"
        Me.BtnRegister.Size = New System.Drawing.Size(123, 42)
        Me.BtnRegister.TabIndex = 4
        Me.BtnRegister.Text = "Register"
        Me.BtnRegister.UseVisualStyleBackColor = True
        '
        'btnLogin
        '
        Me.btnLogin.Location = New System.Drawing.Point(31, 104)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(123, 42)
        Me.btnLogin.TabIndex = 3
        Me.btnLogin.Text = "Login"
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'textPassword
        '
        Me.textPassword.Location = New System.Drawing.Point(92, 51)
        Me.textPassword.MaxLength = 20
        Me.textPassword.Name = "textPassword"
        Me.textPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.textPassword.Size = New System.Drawing.Size(194, 20)
        Me.textPassword.TabIndex = 2
        Me.textPassword.Text = "test"
        Me.textPassword.UseSystemPasswordChar = True
        '
        'textUsername
        '
        Me.textUsername.Location = New System.Drawing.Point(92, 26)
        Me.textUsername.MaxLength = 20
        Me.textUsername.Name = "textUsername"
        Me.textUsername.Size = New System.Drawing.Size(194, 20)
        Me.textUsername.TabIndex = 1
        Me.textUsername.Text = "test"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Password:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Username:"
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.ForeColor = System.Drawing.Color.Red
        Me.lblError.Location = New System.Drawing.Point(92, 85)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(147, 13)
        Me.lblError.TabIndex = 10
        Me.lblError.Text = "Invalid username or password"
        Me.lblError.Visible = False
        '
        'loginProgress
        '
        Me.loginProgress.Location = New System.Drawing.Point(31, 75)
        Me.loginProgress.Name = "loginProgress"
        Me.loginProgress.Size = New System.Drawing.Size(255, 23)
        Me.loginProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.loginProgress.TabIndex = 11
        Me.loginProgress.Visible = False
        '
        'loginWorker
        '
        '
        'RegisterWorker
        '
        '
        'LoginUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.loginProgress)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.textUsername)
        Me.Controls.Add(Me.textPassword)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.BtnRegister)
        Me.Name = "LoginUI"
        Me.Size = New System.Drawing.Size(320, 164)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnRegister As Button
    Friend WithEvents btnLogin As Button
    Friend WithEvents textPassword As TextBox
    Friend WithEvents textUsername As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblError As Label
    Friend WithEvents loginProgress As ProgressBar
    Friend WithEvents loginWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents RegisterWorker As System.ComponentModel.BackgroundWorker
End Class
