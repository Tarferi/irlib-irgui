﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IRectLib.Impl {
    class IRServerImpl : IRDisplayableImpl, IREventHandler, IRServer {

        private IRCryptoObject crypto;

        private IRCallbackCollection callbacks;
        private HTTPConnector hcon;
        private IRCConnector icon;
        private IRInstanceImpl instance;

        private String sessionKey;
        private String IRServer;
        private String IRUsername;
        private String IRChannel = null;
        private static String DefaultIRChannel = "IRProtocol_dev";

        private IList<IRUser> friends = new List<IRUser>();

        public void addFriend(IRUser friend) {
            friends.Add(friend);
        }

        public IRServerImpl(IRInstanceImpl instance, IRLoginRequestResultImpl loginRes, IRCallbackCollection callbacks) {
            hcon = instance.getHTTPConnector();
            this.crypto = loginRes.getCryptoObject();
            icon = new IRCConnector(this);
            this.callbacks = callbacks;
            this.instance = instance;
            this.sessionKey = loginRes.getSessionKey();
            this.IRUsername = loginRes.getMyUsername();
            this.IRServer = loginRes.getServerIP();
            this.IRChannel = loginRes.getIRChannel();
            IRChannel = IRChannel == null ? DefaultIRChannel : IRChannel;
            icon.connect(IRServer, IRUsername, IRChannel);
        }

        public void reportOnlines(IList<IRUserOnlineImpl> lst) {
            IRReportRequestResultImpl rep=hcon.fireReport(sessionKey, lst);
            if(rep.isOk()) {

            }
        }

        public void notifyUserChange(IRUserImpl u) {
            callbacks.getIRUserStatusChangeListener().IRUserStatusChanged(u, u.getStatus());
        }

        public void notifyFriendshipRequest(IRUserImpl u) {
            callbacks.getIRFrienshipRequestListener().IRFriendshipRequestReceived(new IRFriendshipRequestImpl(this, u));
        }

        public void acceptFriendship(IRUserImpl user) {
            hcon.setFriendship(sessionKey, user.getUsername(), true);
        }

        public void rejectFriendship(IRUserImpl user) {
            hcon.setFriendship(sessionKey, user.getUsername(), false);
        }

        public IList<IRUser> getAllFriends() {
            return friends;
        }

        public IList<IRUser> searchUser(string usernameOrDisplayName) {
            return hcon.searchUser(usernameOrDisplayName);
        }

        public void setMyStatus(IRUserStatus myStatus) {
            icon.broadcastMyStatus(myStatus);
        }

        public void handleEvent(IREvent e) {
            throw new NotImplementedException();
        }
    }
}
