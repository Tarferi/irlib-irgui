﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IRectLib.Impl {

    class HTTPConnector {

        // private static String URL_BASE = "http://127.0.0.1:8000/database.php";

        private static String URL_BASE = "http://ir.ithief.org/";

        private static String URL_LOGIN = URL_BASE + "?action=auth&username=%1&password=%2&key=%3";

        private static String URL_UPDATE_PROFILE = URL_BASE + "?action=updateProfile&key=%1&description=%2";

        private static String URL_FRIENDSHIP_UPDATE = URL_BASE + "?action=setfriendship&key=%1&target=%2&status=%3";

        private static String URL_REPORT = URL_BASE + "?action=privateReport&key=%1&target=%2";

        private static String URL_REGISTER = URL_BASE + "?action=register&username=%1&password=%2";

        private static object getPage(String url) {
            try {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();
                using (StreamReader reader = new StreamReader(resStream, Encoding.UTF8)) {
                    String str = reader.ReadToEnd();
                    dynamic stuff = JsonConvert.DeserializeObject(str);
                    return stuff;
                }
            } catch (Exception e) {
                return "";
            }
        }

        private static String handleInsertions(String data, params string[] replacements) {
            for (int i = replacements.Length - 1; i >= 0; i--) {
                data = data.Replace("%" + (i+1).ToString(), System.Uri.EscapeUriString(replacements[i]));
            }
            return data;
        }

        private static String cannotBeEmpty(String str, String defaultStr) {
            if(str == null) {
                return defaultStr;
            }
            if(str.Length == 0) {
                return defaultStr;
            }
            return str;
        }


        internal IList<IRUser> searchUser(string usernameOrDisplayName) {
            throw new NotImplementedException();
        }

        public IRLoginRequestResult tryLogin(IRInstanceImpl instance, IRCallbackCollection callbacks, String username, String password) {
            IRCryptoObject crypto = new IRCryptoObject();
            return new IRLoginRequestResultImpl(getPage(handleInsertions(URL_LOGIN, cannotBeEmpty(username,"a"), cannotBeEmpty(password,"a"), cannotBeEmpty(crypto.getMyKey(),"a"))), instance, callbacks, crypto);
        }

        public IRRegisterRequestResult register(String username, String password) {
            return new IRRegisterRequestResultImpl(getPage(handleInsertions(URL_REGISTER, cannotBeEmpty(username,"a"), cannotBeEmpty(password,"a"))));
        }

        internal IRFriendshipRequestResultImpl setFriendship(string myKey, string username, bool add) {
            return new IRFriendshipRequestResultImpl(getPage(handleInsertions(URL_FRIENDSHIP_UPDATE, cannotBeEmpty(myKey, "a"), cannotBeEmpty(username, "a"), add?"add":"remove")));
        }

        internal IRReportRequestResultImpl fireReport(String myKey, IList<IRUserOnlineImpl> users) {
            String rep = "";
            foreach(IRUserOnlineImpl onl in users) {
                rep += onl.getIRSession() + ",";
            }
            if(rep.Length > 0) {
                rep = rep.Substring(0, rep.Length - 1);
            }
            return new IRReportRequestResultImpl(getPage(handleInsertions(URL_REPORT, cannotBeEmpty(myKey, "a"), cannotBeEmpty(rep, "a"))));
        }

    }

    class IRReportRequestResultImpl : IRRequestResultImpl, IRRequestResult {
        public IRReportRequestResultImpl(dynamic obj) : base((object)obj) {
            if (base.isOk()) {
                dynamic data = base.getDynamicData();
                if (data is String) {
                    switch ((String)data) {
                        case "Ok": // Good request
                            break;
                        default:
                            base.setError(true);
                            base.setErrorString("Unknown error");
                            break;
                    }
                }
            }
        }
    }

    class IRFriendshipRequestResultImpl : IRRequestResultImpl, IRRequestResult {
        public IRFriendshipRequestResultImpl(dynamic obj) : base((object)obj) {
            if (base.isOk()) {
                dynamic data = base.getDynamicData();
                if (data is String) {
                    switch ((String)data) {
                        case "Ok": // Good request
                            break;
                        default:
                            base.setError(true);
                            base.setErrorString("Unknown error");
                            break;
                    }
                }
            }
        }
    }

    class IRRequestResultImpl:IRRequestResult {

        private Boolean ok;
        private int code;
        private dynamic errorString;

        public int getCode() {
            return code;
        }

        public Boolean isOk() {
            return ok;
        }

        public String getErrorString() {
            return (String)errorString;
        }

        public dynamic getDynamicData() {
            return errorString;
        }

        public IRRequestResultImpl(dynamic obj) {
            try {
                String s = obj.STATUS;
                ok = s.Equals("Ok");
                errorString = obj.DETAILS;
                code = obj.CODE;
            } catch (Exception e) {
                ok = false;
                errorString = "Invalid server response";
            }
        }

        internal void setError(bool v) {
            ok = !v;
        }

        internal void setErrorString(string v) {
            errorString = v;
        }
    }

    class IRRegisterRequestResultImpl : IRRequestResultImpl, IRRegisterRequestResult {

        public IRRegisterRequestResultImpl(dynamic obj) : base((object)obj) {
            if (base.isOk()) {
                dynamic data = base.getDynamicData();
                if (data is String) {
                    switch ((String)data) {
                        case "Code 0": // Good registration
                            break;
                        case "Code 1":
                            base.setError(true);
                            base.setErrorString("Username is already registered");
                            break;
                        case "Code 5":
                            base.setError(true);
                            base.setErrorString("Invalid username");
                            break;
                        case "Code 6":
                            base.setError(true);
                            base.setErrorString("Invalid password");
                            break;
                        default:
                            base.setError(true);
                            base.setErrorString("Unknown error");
                            break;
                    }
                }
            }
        }
    }

    class IRLoginRequestResultImpl : IRRequestResultImpl, IRLoginRequestResult {

        private IRServerImpl server;

        private IRCryptoObject crypto;

        private String myUsername;
        private String ServerIP;
        private String sessionKey;
        private String channel = null;

        private IList<IRUser> friends = new List<IRUser>();

        public String getSessionKey() {
            return sessionKey;
        }

        public String getMyUsername() {
            return myUsername;
        }

        public String getServerIP() {
            return ServerIP;
        }

        public IList<IRUser> getFriends() {
            return friends;
        }

        public IRLoginRequestResultImpl(dynamic obj, IRInstanceImpl instance, IRCallbackCollection callbacks, IRCryptoObject crypto) : base((object)obj) {
            this.crypto = crypto;
            if (base.isOk()) {
                dynamic data = base.getDynamicData();
                if (data is String) {
                    switch ((String)data) {
                        case "Code 1":
                        case "Code 3":
                            base.setError(true);
                            base.setErrorString("Database exception");
                            break;
                        case "Code 2":
                            base.setError(true);
                            base.setErrorString("Invalid username or password");
                            break;
                        case "Code 4":
                            base.setError(true);
                            base.setErrorString("Invalid login key. Please try it again");
                            break;
                        case "Code 5":
                            base.setError(true);
                            base.setErrorString("Invalid username");
                            break;
                        case "Code 6":
                            base.setError(true);
                            base.setErrorString("Invalid password");
                            break;
                        default:
                            base.setError(true);
                            base.setErrorString("Unknown error");
                            break;
                    }
                } else if (data is JObject) {
                    base.setError(false);
                    myUsername = data.Me.Username;
                    ServerIP = data.Me.Server;
                    sessionKey = data.Me.Session;
                    var friends = data.Friends;
                    server = new IRServerImpl(instance, this, callbacks);
                    for(int i=0;i<friends.Count;i++) {
                        var friend = friends[i];
                        String friendUsername = friend.Username;
                        String weAreFriends = friend.Friends;
                        var Onlines = friend.Onlines;
                        String sbs = friend.Subs;
                        IRSubscription subs=IRSubscription.NONE;
                        if(sbs.Equals("None")) {
                            subs = IRSubscription.NONE;
                        } else if (sbs.Equals("Me")) {
                            subs = IRSubscription.ME;
                        } else if (sbs.Equals("Them")) {
                            subs = IRSubscription.THEM;
                        } else if (sbs.Equals("Both")) {
                            subs = IRSubscription.BOTH;
                        } else {
                            continue;
                        }
                        IRUserImpl user = new IRUserImpl(instance, server, friendUsername, friendUsername, "", subs);
                        server.addFriend(user);
                        for(int o=0;o<Onlines.Count;o++) {
                            var Online = Onlines[i];
                            String IRServer = Online.IRServer;
                            String IRName = Online.IRName;
                            String IRKey = Online.IRKey;
                            String IRSession = Online.IRSession;
                            user.addOnline(new IRUserOnlineImpl(IRKey, IRName, IRServer, IRSession));
                        }
                    }
                }
            }
        }

        public IRCryptoObject getCryptoObject() {
            return crypto;
        }

        public IRServer getIRServer() {
            return server;
        }

        internal String getIRChannel() {
            return channel;
        }
    }
}