﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRectLib.Impl {
    class IRDisplayableImpl : IRDisplayable {

        private IRDataChangedListener displayObject;

        public object getDisplayObject() {
            return displayObject;
        }

        public void setDisplayObject(IRDataChangedListener displayObject) {
            this.displayObject = displayObject;
        }
    }
}
