﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IRectLib.Impl {
    class IRCryptoObject {
        private String myKey;
        private string privateKey;
        private string publicKey;
        private RSACryptoServiceProvider rsa;

        private static UnicodeEncoding _encoder = new UnicodeEncoding();

        public String getMyKey() {
            return myKey;
        }

        public void regenKey() {
            rsa = new RSACryptoServiceProvider();
            privateKey = rsa.ToXmlString(true);
            publicKey = rsa.ToXmlString(false);
            myKey = System.Convert.ToBase64String(Encoding.Unicode.GetBytes(publicKey));
        }

        public String encryptMessage(String message) {
            var dataToEncrypt = _encoder.GetBytes(message);
            byte[] encryptedByteArray = rsa.Encrypt(dataToEncrypt, false).ToArray();
            return System.Convert.ToBase64String(encryptedByteArray);
        }

        public String decryptMessage(String mesasge) {
            char[] dataArray = mesasge.ToArray();
            byte[] dataByte = new byte[dataArray.Length];
            for (int i = 0; i < dataArray.Length; i++) {
                dataByte[i] = Convert.ToByte(dataArray[i]);
            }
            var decryptedByte = rsa.Decrypt(dataByte, false);
            String baseresult = _encoder.GetString(decryptedByte);
            var base64EncodedBytes = System.Convert.FromBase64String(baseresult);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);

        }

        public IRCryptoObject() {
            regenKey();
        }
    }
}
