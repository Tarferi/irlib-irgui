﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sharkbite.Irc;

namespace IRectLib.Impl {

    class IRCConnector {

        private Connection connection;
        private IREventHandler handler;

        public IRCConnector(IREventHandler handler) {
            this.handler = handler;
        }

        public void connect(String server, String username, String channel) {
            //Identd.Start(username);
            ConnectionArgs cargs = new ConnectionArgs(username, server);
            connection = new Connection(cargs, false, false);
            connection.Listener.OnRegistered += new RegisteredEventHandler(_OnRegistered);
            connection.Listener.OnPublic += new PublicMessageEventHandler(_OnPublicMessage);
            connection.Listener.OnPrivate += new PrivateMessageEventHandler(_OnPrivateMessage);
            connection.Listener.OnError += new ErrorMessageEventHandler(_OnError);
            connection.Listener.OnDisconnected += new DisconnectedEventHandler(_OnDisconnected);
            connection.Listener.OnNick += new NickEventHandler(_OnNichChanged);
        }

        internal void broadcastMyStatus(IRUserStatus myStatus) {
            throw new NotImplementedException();
        }

        private void _OnNichChanged(UserInfo user, string newNick) {
            handler.handleEvent(new IRNickChangeEvent(this, user.Nick, newNick));
        }

        public void _OnRegistered() {
            handler.handleEvent(new IRConnectEvent(this));
        }

        public void _OnPublicMessage(UserInfo user, string channel, string message) {
            handler.handleEvent(new IRPublicMessage(this, message, user.Nick, channel));
        }
        public void _OnPrivateMessage(UserInfo user, string message) {
            handler.handleEvent(new IRPrivateMessage(this, message, user.Nick));
        }
        public void _OnError(ReplyCode code, string message) {
            connection.Disconnect("Unexpected expectations");
        }
        public void _OnDisconnected() {
            handler.handleEvent(new IRDisconnectEvent(this));
        }
    }


    abstract class IREvent {

        private IRCConnector con;

        protected IREvent(IRCConnector con) {
            this.con = con;
        }

    }

    class IRConnectEvent : IREvent {
        public IRConnectEvent(IRCConnector con) : base(con) {

        }
    }

    class IRDisconnectEvent : IREvent {
        public IRDisconnectEvent(IRCConnector con) : base(con) {

        }
    }

    class IRNickChangeEvent : IREvent {
        private string oldNick;
        private string newNick;

        public IRNickChangeEvent(IRCConnector con, string oldNick, string newNick) : base(con) {
             this.oldNick = oldNick;
            this.newNick = newNick;
        }

        public String getOldNick() {
            return oldNick;
        }

        public String getNewNick() {
            return newNick;
        }
    }

    class IRPrivateMessage : IREvent {
        private String msg;
        private String author;

        public IRPrivateMessage(IRCConnector con, String message, String author) : base(con) {
            this.msg = message;
            this.author = author;
        }

        public String getMessage() {
            return msg;
        }

        public String getAuthor() {
            return author;
        }

    }

    class IRPublicMessage : IRPrivateMessage {
        private String channel;

        public IRPublicMessage(IRCConnector con, string message, string author, string channel) : base(con, message, author) {
            this.channel = channel;
        }

        public String getChannel() {
            return channel;
        }
    }

    interface IREventHandler {

        void handleEvent(IREvent e);

    }
}
