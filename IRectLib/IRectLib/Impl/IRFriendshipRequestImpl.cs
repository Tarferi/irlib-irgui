﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRectLib.Impl {
    class IRFriendshipRequestImpl : IRFriendshipRequest {

        private IRServerImpl server;
        private IRUserImpl user;

        public IRFriendshipRequestImpl(IRServerImpl server,IRUserImpl user) {
            this.server = server;
            this.user = user;
        }

        public void acceptRequest() {
            server.acceptFriendship(user);
        }

        public void declineRequest() {
            server.rejectFriendship(user);
        }

        public IRUser getRequestSourceUser() {
            return user;
        }
    }
}
