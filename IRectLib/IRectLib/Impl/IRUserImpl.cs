﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace IRectLib.Impl {


    class IRUserOnlineImpl {
        private String IRKey;
        private String IRName;
        private String IRServer;
        private String IRSession;

        public IRUserOnlineImpl(String IRKey, String IRName, String IRServer, String IRSession) {
            this.IRKey = IRKey;
            this.IRName = IRName;
            this.IRServer = IRServer;
            this.IRSession = IRSession;
        }

        public String getIRKey() {
            return IRKey;
        }

        public String getIRName() {
            return IRName;
        }

        public String getIRServer() {
            return IRServer;
        }

        public String getIRSession() {
            return IRSession;
        }

    }

    class IRUserImpl : IRDisplayableImpl, IRUser {

        private IList<IRUserOnlineImpl> onlines = new List<IRUserOnlineImpl>();

        private IRInstanceImpl instance;
        private IRServerImpl server;

        private ImageSource avatar=IRImageLoader.getDefaultAvatar();
        private String avatarURL="";
        private IRChatImpl chat;

        private String description="";
        private String displayName;
        private String username;

        private DateTime lastMessageTime = DateTime.Now;
        private IRUserStatus status = IRUserStatus.OFFLINE;
        private IRSubscription subscription = IRSubscription.NONE;

        public void addOnline(IRUserOnlineImpl online) {
            onlines.Add(online);
            server.notifyUserChange(this);
        }

        public void removeOnline(String username) {
            IRUserOnlineImpl target = null;
            foreach(IRUserOnlineImpl online in onlines) {
                if(online.getIRName().Equals(username)) {
                    target = online;
                }
            }
            if (target != null) {
                onlines.Remove(target);
            }
            if(onlines.Count == 0) { // Not online anymore
                status = IRUserStatus.OFFLINE;
            }
            server.notifyUserChange(this);
        }

        public IRUserImpl(IRInstanceImpl instance, IRServerImpl server, String username, String displayName, String description, IRSubscription subscription) {
            this.instance = instance;
            this.server = server;
            this.username = username;
            this.displayName = displayName;
            this.subscription = subscription;
            this.chat = new IRChatImpl(instance, server, this);
        }

        public void setDescription(string newDesc) {
            description = newDesc;
        }

        public ImageSource getAvatar() {
            return avatar;
        }

        public string getAvatarURL() {
            return avatarURL;
        }

        public IRChat getChat() {
            return chat;
        }

        public string getDescription() {
            return description;
        }

        public string getDisplayName() {
            return displayName;
        }

        public DateTime getLastMessageDate() {
            return lastMessageTime;
        }

        public IRUserStatus getStatus() {
            return status;
        }

        public ImageSource getStatusIcon() {
            return IRImageLoader.getStatusIcon(status);
        }

        public IRSubscription getSubscription() {
            return subscription;
        }

        public string getUsername() {
            return username;
        }

        public void sendFriendshipRequest() {
            instance.sendFriendshipRequest();
        }
    }
}
