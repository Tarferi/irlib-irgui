﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IRectLib.Impl {

    class IRInstanceImpl : IRInstance {

        private HTTPConnector hcon = new HTTPConnector();


        public IRInstanceImpl() {
            
        }


        public HTTPConnector getHTTPConnector() {
            return hcon;
        }

        IRLoginRequestResult IRInstance.tryLogin(string username, string password, IRCallbackCollection callbacks) {
            return hcon.tryLogin(this, callbacks, username, password);
        }

   
        IRRegisterRequestResult IRInstance.tryRegister(string username, string password) {
            return hcon.register(username, password);
        }

        internal void sendPrivateMessage(string toUser, string message) {
            //TODO:
        }

        internal void sendFriendshipRequest() {
            //TODO:
        }

    }
}
