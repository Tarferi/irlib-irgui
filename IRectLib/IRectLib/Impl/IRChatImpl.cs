﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRectLib.Impl {
    class IRChatImpl : IRDisplayableImpl, IRChat {
        private IRInstanceImpl instance;
        private IRServerImpl server;

        private IRUserImpl user;
        private String chatName;

        public IRChatImpl(IRInstanceImpl instance, IRServerImpl server, IRUserImpl user) {
            this.instance = instance;
            this.server = server;
            this.user = user;
            this.chatName = user.getDisplayName();
        }

        public string getChatName() {
            return chatName;
        }
  
        public IRUser getUser() {
            return user;
        }

        public void sendMessage(string message) {
            this.instance.sendPrivateMessage(user.getUsername(), message);
        }

        public void setChatName(string newName) {
            chatName = newName;
        }

    }
}
