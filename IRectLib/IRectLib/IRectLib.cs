﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace IRectLib {

    /**
     * 
     * Use when creating IR instance. Do not create instance any other way
     * 
     */
    public class IRFactory {
        
        /*
         * 
         * Creates IR instance
         * 
         */
        public static IRInstance createInstance() {
            //return new Example.IRnstanceExample();
            return new Impl.IRInstanceImpl();
        }

    }

    /*
     * 
     * All IR callbacks should be available prior connecting to server 
     *
     */
    public class IRCallbackCollection {
        private IRMessageListener mListener;
        private IRFrienshipRequestListener fListener;
        private IRUserStatusChangeListener uListener;

        public IRCallbackCollection(IRMessageListener mListener, IRFrienshipRequestListener fListener, IRUserStatusChangeListener uListener) {
            this.mListener = mListener;
            this.fListener = fListener;
            this.uListener = uListener;
        }

        public IRMessageListener getIRMessageListener() {
            return mListener;
        }

        public IRFrienshipRequestListener getIRFrienshipRequestListener() {
            return fListener;
        }

        public IRUserStatusChangeListener getIRUserStatusChangeListener() {
            return uListener;
        }

    }

    public interface IRRequestResult {

        bool isOk();

        String getErrorString();

    }

    public interface IRLoginRequestResult:IRRequestResult {
        IRServer getIRServer();
    }

    public interface IRRegisterRequestResult:IRRequestResult {

    }

    public interface IRInstance {

        IRLoginRequestResult tryLogin(string username, String password, IRCallbackCollection callbacks);

        IRRegisterRequestResult tryRegister(string username, String password);
    }

    public interface IRServer : IRDisplayable {

        IList<IRUser> searchUser(String usernameOrDisplayName);

        IList<IRUser> getAllFriends();

        void setMyStatus(IRUserStatus myStatus);

    }

    public interface IRFrienshipRequestListener {
        void IRFriendshipRequestReceived(IRFriendshipRequest request);
    }

    public interface IRUserStatusChangeListener {

        void IRUserStatusChanged(IRUser user, IRUserStatus newStatus);

    }

    public interface IRDisplayable {

        Object getDisplayObject();

        void setDisplayObject(IRDataChangedListener displayObject);

    }

    public interface IRUser : IRDisplayable {

        String getUsername();

        String getDisplayName();

        String getDescription();

        String getAvatarURL();

        IRUserStatus getStatus();

        IRChat getChat();

        DateTime getLastMessageDate();

        IRSubscription getSubscription();

        ImageSource getStatusIcon();

        ImageSource getAvatar();

        void sendFriendshipRequest();

    }

    public interface IRFriendshipRequest {

        IRUser getRequestSourceUser();

        void acceptRequest();

        void declineRequest();

    }

    public enum IRSubscription {
        BOTH = 0,
        ME,
        THEM,
        NONE
    }

    public enum IRUserStatus {
        ONLINE = 0,
        OFFLINE,
        AWAY,
        BUSY
    }

    public interface IRChat : IRDisplayable {

        IRUser getUser();

        String getChatName();

        void setChatName(String newName);

        void sendMessage(String message);

    }

    public interface IRMessageListener {

        void IRMessageReceived(IRChat chat, IRUser sender, DateTime receiveTime, String messageContents);

    }

    public interface IRDataChangedListener {

        void dataChanged(IRDisplayable data);

    }

    public class IRObject {

        private Object content;

        public IRObject(Object content) {
            this.content = content;
        }

        bool isNull() {
            return content == null;
        }

        IRServer asIRServer() {
            return (IRServer)content;
        }

        IList<IRUser> asListOfUsers() {
            return (IList<IRUser>)content;
        }

        String asString() {
            return (String)content;
        }

        bool asBoolean() {
            return (bool)content;
        }

    }

    public class IRImageLoader {

        private static ImageSource defaultIcon;
        private static ImageSource[] statuses;

        static IRImageLoader() {
            defaultIcon = loadImageByName("defaultUser.png");
            statuses = new ImageSource[Enum.GetNames(typeof(IRUserStatus)).Length];
            statuses[(int)IRUserStatus.AWAY] = loadImageByName("away.png");
            statuses[(int)IRUserStatus.BUSY] = loadImageByName("busy.png");
            statuses[(int)IRUserStatus.OFFLINE] = loadImageByName("offline.png");
            statuses[(int)IRUserStatus.ONLINE] = loadImageByName("online.png");

            defaultIcon.Freeze();
            statuses[0].Freeze();
            statuses[1].Freeze();
            statuses[2].Freeze();
            statuses[3].Freeze();

        }

        private static ImageSource loadImageByName(String name) {
            System.Reflection.Assembly myAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            Stream myStream = myAssembly.GetManifestResourceStream("IRectLib.images." + name);
            Bitmap image = new Bitmap(myStream);
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(image.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }

        private IRImageLoader() { }

        public static ImageSource getDefaultAvatar() {
            return defaultIcon;
        }

        public static ImageSource loadImageForUser(IRUser user) {
            return defaultIcon;
        }

        public static ImageSource getStatusIcon(IRUserStatus status) {
            return statuses[(int)status];
        }

    }
}
