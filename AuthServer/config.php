<?php
class Config {
	const DATABASE_IP = "127.0.0.1";
	const DATABASE_USERNAME = "root";
	const DATABASE_PASSWORD = "5";
	const DATABASE_NAME = "irauth";
	const DATABASE_TABLE_PREFIX = "irauth_";
	const SERVER_IP = "irc.anonnet.org";
	const LOGIN_EXPIRE = 60; // 1 minute
}