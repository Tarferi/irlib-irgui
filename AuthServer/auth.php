<?php
include_once "config.php";

/**
 * Config.php contents:
 *
 * class Config {
 * const DATABASE_IP = "127.0.0.1";
 * const DATABASE_USERNAME = "root";
 * const DATABASE_PASSWORD = "5";
 * const DATABASE_NAME = "irauth";
 * const DATABASE_TABLE_PREFIX = "irauth_";
 * const SERVER_IP = "irc.anonnet.org";
 * const LOGIN_EXPIRE = 60; // 1 minute
 * }
 */

/**
 * Calls:
 *
 * action=auth&username=<username>&password=<password>&key=<key>
 * - Attemps login using <username> and <password>. Both are length limited to 20 characters. <key> is Base64 of public key limited to 50 characters. They key must be exclusive on entire server, so generation is recommended.
 * - If given key is already present on server, return code is 0 and return data is "Code 4"
 * - This does not make you visible to your friends. As soon as you are ready on the given IRC server, send GOLIVE action
 * - This action returns key that will be used for further authentication. Using <key> is deprecated
 *
 * action=golive&key=<key>
 * - Sets your server status to online, so all your new friends can see you online. You must, however make current users aware of your status
 * - This must follow LOGIN action in less than 60 seconds, otherwise new LOGIN action is required
 *
 * action=register&username=<username>&password=<password>
 * - Registers the account. Both are length limited to range (4-20). This action will not commit login
 *
 *
 * action=privateReport&key=<key>&target=<target>
 * - Reports <target> as offline user(s). If CSV array is provided, then all members of given array are removed from online list.
 *
 *
 * action=setfriendship&key=<key>&target=<target>&status=<status>
 * - Modifies friendship status, given YOUR key in <key>, target USERNAME in <target> and status in <status>
 * - Possible <status> values are only "add", "remove", "block"
 *
 *
 * action=install
 * - Use this to create tables. This will not purge existing data and will not project changes to existing tables
 *
 * action=uninstall
 * - Use this to remove tables. This will remove everything including table data
 *
 * action=reinstall
 * - Use this to project changes in table structures. This will commit uninstall and then install, so all table data will be removed
 */
abstract class Database {
	private $link = false;
	private $selectCount = 0;
	private $executeCount = 0;
	public function toTableName($str) {
		return Config::DATABASE_TABLE_PREFIX . $str;
	}
	public function getSelectQueriesCount() {
		return $this->selectCount;
	}
	public function getExecuteQueriesCount() {
		return $this->executeCount;
	}
	public function getTotalQueriesCount() {
		return $this->selectCount + $this->executeCount;
	}
	public function selectQuery($query, $replacements) {
		if ($this->link === false) {
			$this->link = $this->handleInitDatabase ();
			if ($this->link === false || $this->link === null) {
				throw new DatabaseException ();
			}
		}
		$query = $this->handleReplacements ( $this->link, $query, $replacements );
		if ($query != false) {
			$this->selectCount ++;
			// var_dump($query);
			// echo $query . "<br /><br />";
			return $this->commitSelectQuery ( $this->link, $query );
		}
		return false;
	}
	public function createTables() {
		$this->executeQuery ( "
											
							CREATE TABLE IF NOT EXISTS ? (
							  id int(11) NOT NULL AUTO_INCREMENT,
							  username varchar(20) COLLATE utf8_bin NOT NULL,
							  password varchar(20) COLLATE utf8_bin NOT NULL,
							  description text COLLATE utf8_bin NOT NULL,
							  PRIMARY KEY (`id`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
				", array (
				$this->toTableName ( "users" ) 
		) );
		$this->executeQuery ( "
				
							CREATE TABLE IF NOT EXISTS ? (
							  id int(11) NOT NULL AUTO_INCREMENT,
							  user1_id int(11) NOT NULL,
							  user2_id int(11) NOT NULL,
							  user1_subs int(11) NOT NULL,
							  user2_subs int(11) NOT NULL,
							  PRIMARY KEY (`id`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
				", array (
				$this->toTableName ( "friendships" ) 
		) );
		$this->executeQuery ( "
							
							CREATE TABLE IF NOT EXISTS ? (
							  id int(11) NOT NULL AUTO_INCREMENT,
							  user_id int(11) NOT NULL,
							  user_key text COLLATE utf8_bin NOT NULL,
							  user_server text COLLATE utf8_bin NOT NULL,
							  user_username text COLLATE utf8_bin NOT NULL,
							  user_logintime int(11) NOT NULL,
							  user_logged int(2) NOT NULL,
						      user_lkey varchar(20) COLLATE utf8_bin NOT NULL,
							  PRIMARY KEY (`id`),
							  KEY `user_id` (`user_id`),
							  CONSTRAINT `irauth_onlines_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `irauth_users` (`id`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


				", array (
				
				$this->toTableName ( "onlines" ) 
		) );
	}
	public function deleteTables() {
		$this->executeQuery ( "DROP TABLE IF EXISTS ?;DROP TABLE IF EXISTS ?; DROP TABLE IF EXISTS ?", array (
				$this->toTableName ( "users" ),
				$this->toTableName ( "friendships" ),
				$this->toTableName ( "onlines" ) 
		) );
	}
	public function recreateTables() {
		$this->deleteTables ();
		$this->createTables ();
	}
	protected abstract function handleInitDatabase();
	protected abstract function handleReplacements($link, $query, $replacements);
	protected abstract function commitExecuteQuery($link, $query);
	protected abstract function commitSelectQuery($link, $query);
	public function executeQuery($query, $replacements) {
		if ($this->link === false) {
			$this->link = $this->handleInitDatabase ();
			if ($this->link === false || $this->link === null) {
				throw new DatabaseException ();
			}
		}
		$query = $this->handleReplacements ( $this->link, $query, $replacements );
		if ($query != false) {
			$this->executeCount ++;
			// var_dump($query);
			// echo $query . "<br /><br />";
			return $this->commitExecuteQuery ( $this->link, $query );
		}
		return false;
	}
}
class MySQL extends Database {
	protected function handleInitDatabase() {
		$b = @new mysqli ( Config::DATABASE_IP, Config::DATABASE_USERNAME, Config::DATABASE_PASSWORD, Config::DATABASE_NAME );
		if ($b) {
			if (@$b->server_info != null) {
				$b->set_charset ( "utf8" );
				return $b;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	protected function handleReplacements($link, $query, $replacements) {
		$b = explode ( "?", $query );
		if (count ( $b ) != count ( $replacements ) + 1) {
			throw new DatabaseException ();
		} else {
			$totalstr = array ();
			for($i = 0, $o = count ( $replacements ); $i < $o; $i ++) {
				$totalstr [] = $b [$i];
				$r = $link->real_escape_string ( $replacements [$i] );
				$totalstr [] = $r;
			}
			$totalstr [] = $b [count ( $b ) - 1];
		}
		return implode ( "", $totalstr );
	}
	protected function commitExecuteQuery($link, $query) {
		try {
			$link->query ( $query );
		} catch ( Exception $e ) {
			throw new DatabaseException ();
		}
	}
	protected function commitSelectQuery($link, $query) {
		try {
			$result = $link->query ( $query );
			if (! $result) {
				throw new DatabaseException ();
			}
			$res = array ();
			while ( $obj = $result->fetch_object () ) {
				$res [] = $obj;
			}
			$result->free ();
			return $res;
		} catch ( Exception $e ) {
			throw new DatabaseException ();
		}
	}
}
class AsrtException extends Exception {
}
class Subscription {
	private $add = false;
	private $remove = false;
	private $valid = true;
	public function isAdd() {
		return $this->add;
	}
	public function isRemove() {
		return $this->remove;
	}
	public function isValid() {
		return $this->valid;
	}
	public function __construct($str) {
		if ($str == "add") {
			$this->add = true;
		} else if ($str == "remove") {
			$this->remove = true;
		} else {
			$this->valid = false;
		}
	}
	const UNKNOWN = 1;
	const FRIEND = 2;
	const BLOCK = 3;
}
class AuthRequeser {
	public function __construct($data) {
		$preq = function ($data, $key, $default = false) {
			if (isset ( $data [$key] )) {
				return $data [$key];
			} else {
				return $default;
			}
		};
		$asrt = function ($data, $key, $default = false) use ($preq) {
			if ($preq ( $data, $key, $default )) {
				return $preq ( $data, $key, $default );
			} else {
				throw new AsrtException ();
			}
		};
		try {
			$action = $asrt ( $data, "action" );
			switch ($action) {
				default :
					throw new AsrtException ();
					break;
				case "install" :
					$db = new MySQL ();
					$db->createTables ();
					die ( json_encode ( array (
							"STATUS" => "Ok",
							"CODE" => 0,
							"DETAILS" => "Database created" 
					) ) );
					break;
				
				case "uninstall" :
					$db = new MySQL ();
					$db->deleteTables ();
					die ( json_encode ( array (
							"STATUS" => "Ok",
							"CODE" => 0,
							"DETAILS" => "Database removed" 
					) ) );
					break;
				
				case "reinstall" :
					$db = new MySQL ();
					$db->deleteTables ();
					$db->createTables ();
					die ( json_encode ( array (
							"STATUS" => "Ok",
							"CODE" => 0,
							"DETAILS" => "Database recreated" 
					) ) );
					break;
				
				case "auth" :
					$username = $asrt ( $data, "username" );
					$password = $asrt ( $data, "password" );
					$key = $asrt ( $data, "key" );
					$req = new IRAuthRequest ( new MySQL (), $username, $password, $key );
					break;
				case "register" :
					$username = $asrt ( $data, "username" );
					$password = $asrt ( $data, "password" );
					$req = new IRRegisterRequest ( new MySQL (), $username, $password );
					break;
				case "privateReport" :
					$myKey = $asrt ( $data, "key" );
					$targetKey = $asrt ( $data, "target" );
					$targetKeys = explode ( ",", $targetKey );
					$targetKeys = count ( $targetKeys ) == 1 ? array (
							$targetKey 
					) : $targetKeys;
					$req = new IRReportRequest ( new MySQL (), $myKey, $targetKeys );
					break;
				case "setfriendship" :
					$myKey = $asrt ( $data, "key" );
					$theFriend = $asrt ( $data, "target" );
					$status = new Subscription ( $asrt ( $data, "status" ) );
					if (! $status->isValid ()) {
						throw new AsrtException ();
					}
					$req = new IRFriendshipStatusUpdate ( new MySQL (), $myKey, $theFriend, $status );
					break;
				case "golive" :
					$key = $asrt ( $data, "key" );
					$req = new IRGoLiveRequest ( new MySQL (), $key );
					break;
			}
		} catch ( AsrtException $e ) {
			die ( json_encode ( array (
					"STATUS" => "Error",
					"CODE" => 1,
					"DETAILS" => "Invalid request" 
			) ) );
		}
		try {
			$result = $req->commit ();
		} catch ( DatabaseException $e ) {
			die ( json_encode ( array (
					"STATUS" => "Error",
					"CODE" => 2,
					"DETAILS" => "Database Error" 
			) ) );
		} catch ( Exception $e ) {
			die ( json_encode ( array (
					"STATUS" => "Error",
					"CODE" => 3,
					"DETAILS" => "Inner server Error" 
			) ) );
		} catch ( Error $e ) {
			die ( json_encode ( array (
					"STATUS" => "Error",
					"CODE" => 3,
					"DETAILS" => "Inner server Error" 
			) ) );
		}
		die ( json_encode ( array (
				"STATUS" => "Ok",
				"CODE" => 0,
				"DETAILS" => $result 
		) ) );
	}
}
class IRFriendshipStatusUpdate extends IRRequest {
	private $myKey;
	private $theFriend;
	private $status;
	public function __construct($db, $myKey, $theFriend, $status) {
		parent::__construct ( $db );
		$this->myKey = $myKey;
		$this->theFriend = $theFriend;
		$this->status = $status;
	}
	protected function innerCommit($db) {
		$users = $db->toTableName ( "Users" );
		$ftable = $db->toTableName ( "friendships" );
		$otable = $db->toTableName ( "onlines" );
		$myId = $db->selectQuery ( "SELECT * FROM ? WHERE user_lkey='?'", array (
				$otable,
				$this->myKey 
		) );
		if (count ( $myId ) != 1) {
			throw new DatabaseException ();
		}
		$myId = $myId [0]->user_id;
		
		$fId = $db->selectQuery ( "SELECT * FROM ? WHERE username='?'", array (
				$users,
				$this->myKey 
		) );
		if (count ( $fId ) != 1) {
			throw new DatabaseException ();
		}
		$fId = $fId [0]->id;
		$res1 = $db->selectQuery ( "SELECT * FROM ? WHERE user1_id = '?' AND user2_id='?'", array (
				$ftable,
				$myId,
				$fId 
		) );
		$res2 = $db->selectQuery ( "SELECT * FROM ? WHERE user1_id = '?' AND user2_id='?'", array (
				$ftable,
				$fId,
				$myId 
		) );
		$res = array_merge ( $res1, $res2 );
		if (count ( $res ) == 0) {
			if ($this->status->isAdd ()) {
				$db->executeQuery ( "INSERT INTO ? (user1_id,user2_id,user1_subs,user2_subs) VALUES (?,?,?,?)", array (
						$ftable,
						$myId,
						$fId,
						Subscription::FRIEND,
						Subscription::UNKNOWN 
				) );
			}
		} else if (count ( $res ) == 1) {
			if (count ( $res1 ) == 1) {
				$theirs = $res1 [0]->user2_subs;
				if ($theirs == Subscription::UNKNOWN && $this->status->isRemove ()) {
					$db->executeQuery ( "DELETE FROM ? WHERE user1_id='?' AND user2_id='?'", array (
							$ftable,
							$myId,
							$fId 
					) );
				} else {
					$db->executeQuery ( "UPDATE ? SET user1_subs='?' WHERE user1_id='?' AND user2_id='?'", array (
							$ftable,
							Subscription::FRIEND,
							$myId,
							$fId 
					) );
				}
			} else {
				$theirs = $res1 [0]->user1_subs;
				if ($theirs == Subscription::UNKNOWN && $this->status->isRemove ()) {
					$db->executeQuery ( "DELETE FROM ? WHERE user2_id='?' AND user1_id='?'", array (
							$ftable,
							$myId,
							$fId 
					) );
				} else {
					$db->executeQuery ( "UPDATE ? SET user1_subs='?' WHERE user2_id='?' AND user1_id='?'", array (
							$ftable,
							Subscription::FRIEND,
							$myId,
							$fId 
					) );
				}
			}
		} else {
			throw new DatabaseException ();
		}
		return "OK";
	}
}
class DatabaseException extends Exception {
}
abstract class IRRequest {
	private $db;
	public function commit() {
		try {
			return $this->innerCommit ( $this->db );
		} catch ( Exception $e ) {
		} catch ( Error $e ) {
		} catch ( Warning $e ) {
		}
		throw new DatabaseException ();
	}
	protected abstract function innerCommit($database);
	public function __construct($db) {
		$this->db = $db;
	}
}
class IRReportRequest extends IRRequest {
	private $myKey;
	private $reportedKeyArray;
	public function __construct($db, $mykey, $reportedKeys) {
		parent::__construct ( $db );
		$this->myKey = $mykey;
		$this->reportedKeyArray = $reportedKeys;
	}
	protected function innerCommit($db) {
		$ftables = $db->toTableName ( "onlines" );
		
		foreach ( $this->reportedKeyArray as $key ) {
			$db->executeQuery ( "DELETE FROM ? WHERE user_lkey = '?'", array (
					$ftables,
					$key 
			) );
		}
		return "OK";
	}
}
class IRRegisterRequest extends IRRequest {
	private $db;
	private $username;
	private $password;
	public function __construct($databaseInstance, $username, $password) {
		parent::__construct ( $databaseInstance );
		$this->username = strtolower ( $username );
		$this->password = $password;
	}
	protected function innerCommit($db) {
		if (strlen ( $this->username ) < 3 || strlen ( $this->username ) > 20) {
			return "Code 5";
		}
		if (strlen ( $this->password ) < 3 || strlen ( $this->password ) > 20) {
			return "Code 6";
		}
		$users = $db->toTableName ( "users" );
		$res = $db->selectQuery ( "SELECT * FROM ? WHERE username='?'", array (
				$users,
				$this->username 
		) );
		if (count ( $res ) == 0) {
			$db->executeQuery ( "INSERT INTO ? (username,password) VALUES ('?','?')", array (
					$users,
					$this->username,
					$this->password 
			) );
			return "Code 0";
		} else {
			return "Code 1";
		}
	}
}
class IRGoLiveRequest extends IRRequest {
	private $key;
	public function __construct($databaseInstance, $key) {
		parent::__construct ( $databaseInstance );
		$this->key = $key;
	}
	protected function innerCommit($db) {
		$otable = $db->toTableName ( "onlines" );
		$db->executeQuery ( "DELETE FROM ? WHERE user_logged = 0 AND user_logintime < ?", array (
				$otable,
				time () - Config::LOGIN_EXPIRE 
		) );
		$res = $db->selectQuery ( "SELECT * FROM ? WHERE user_lkey='?'", array (
				$otable,
				$this->key 
		) );
		if (count ( $res ) != 1) {
			return "Code 1";
		}
		$db->executeQuery ( "UPDATE ? set user_logged = 1 WHERE user_lkey='?'", array (
				$otable,
				$this->key 
		) );
		
		return "Code 0";
	}
}
class IRAuthRequest extends IRRequest {
	private $username;
	private $password;
	private $key;
	public function __construct($databaseInstance, $username, $password, $key) {
		parent::__construct ( $databaseInstance );
		$this->username = $username;
		$this->password = $password;
		$this->key = $key;
	}
	protected function innerCommit($db) {
		$users = $db->toTableName ( "users" );
		$ftable = $db->toTableName ( "friendships" );
		$otable = $db->toTableName ( "onlines" );
		if (strlen ( $this->username ) < 3 || strlen ( $this->username ) > 20) {
			return "Code 5";
		}
		if (strlen ( $this->password ) < 3 || strlen ( $this->password ) > 20) {
			return "Code 6";
		}
		$res = $db->selectQuery ( "select * FROM ? WHERE username='?' AND password='?' LIMIT 1", array (
				$users,
				$this->username,
				$this->password 
		) );
		if (! is_array ( $res )) {
			return "Code 1";
		}
		if (count ( $res ) != 1) {
			return "Code 2";
		}
		$id = $res [0]->id;
		
		$res = $db->selectQuery ( "select * FROM ? WHERE user_key='?' LIMIT 1", array (
				$otable,
				$this->key 
		) );
		if (! is_array ( $res )) {
			return "Code 1";
		}
		if (count ( $res ) != 0) {
			return "Code 4";
		}
		$username = substr ( md5 ( rand () . rand () ), 0, 15 );
		$mySession = substr ( md5 ( $username . "_secret" ), 0, 15 );
		
		$res = $db->executeQuery ( "INSERT INTO ? (user_id,user_key,user_server,user_username,user_logged,user_logintime,user_lkey) VALUES (?,'?','?','?', 0, ?,'?')", array (
				$otable,
				$id,
				$this->key,
				Config::SERVER_IP,
				$username,
				time (),
				$mySession 
		) );
		$res2 = $db->selectQuery ( "
				(
				
						SELECT
						hisUser.username as username,
						onlineTable.user_username as IRName,
						friendTable.user1_subs as mySubs,
						friendTable.user2_subs as hisSubs,
						onlineTable.user_server as IRServer,
						onlineTable.user_key as IRKey,
						onlineTable.user_logged as IRLogged,
						onlineTable.user_lkey as IRSession
						
						FROM ? as friendTable, ? as hisUser
						LEFT JOIN ? as onlineTable
						ON     onlineTable.user_id = hisUser.id
						WHERE
							friendTable.user1_id = ?
						AND	friendTable.user2_id = hisUser.id

				)
				UNION
				(
						SELECT
						hisUser.username as username,
						onlineTable.user_username as IRName,
						friendTable.user1_subs as mySubs,
						friendTable.user2_subs as hisSubs,
						onlineTable.user_server as IRServer,
						onlineTable.user_key as IRKey,
						onlineTable.user_logged as IRLogged,
						onlineTable.user_lkey as IRSession
						
						FROM ? as friendTable, ? as hisUser
						LEFT JOIN ? as onlineTable
						ON     onlineTable.user_id = hisUser.id
						WHERE
							friendTable.user2_id = ?
						AND	friendTable.user1_id = hisUser.id
				)
				ORDER BY username
						", array (
				$ftable,
				$users,
				$otable,
				
				$id,
				
				$ftable,
				$users,
				$otable,
				
				$id 
		) );
		if (! is_array ( $res2 )) {
			return "Code 3";
		}
		$friends = array ();
		foreach ( $res2 as $rs ) {
			$username = $rs->username;
			$mySubs = $rs->mySubs;
			$hisSubs = $rs->hisSubs;
			$IRName = $rs->IRName;
			$IRServer = $rs->IRServer;
			$IRKey = $rs->IRKey;
			$IRLogged = $rh->IRLogged;
			$IRSession = $rh->IRSession;
			$weAreFriends = $mySubs == Subscription::FRIEND && $hisSubs == Subscription::FRIEND;
			$subs = "Blocked";
			if ($mySubs == Subscription::FRIEND) {
				if ($hisSubs == Subscription::FRIEND) {
					$subs = "Both";
				} else if ($hisSubs == Subscription::UNKNOWN) {
					$subs = "Me";
				}
			} else if ($mySubs == Subscription::UNKNOWN) {
				if ($hisSubs == Subscription::FRIEND) {
					$subs = "Them";
				} else if ($hisSubs == Subscription::UNKNOWN) {
					$subs = "None";
				}
			}
			if (! isset ( $friends [$username] )) {
				$friends [$username] = array (
						"Username" => $username,
						"Friend" => $weAreFriends,
						"Subs" => $subs,
						"Onlines" => array () 
				);
			}
			if ($IRKey != null) {
				if ($IRLogged == 1) {
					$friends [$username] ["Onlines"] [] = array (
							"IRServer" => $IRServer,
							"IRName" => $IRName,
							"IRKey" => $IRKey,
							"IRSession" => $IRSession 
					);
				}
			}
		}
		$flist = array ();
		foreach ( $friends as $friend ) {
			$flist [] = $friend;
		}
		$friends = $flist;
		return array (
				"Me" => array (
						"Username" => $username,
						"Server" => Config::SERVER_IP,
						"Session" => $mySession 
				),
				"Friends" => $friends 
		);
	}
}
new AuthRequeser ( $_GET );